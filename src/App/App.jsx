import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { PrivateRoute } from '../_components';
import { alertActions } from '../_actions';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { DashBoard } from "../DashBoard";
import { Patients} from "../Pages/Patients";
import { Doctors } from "../Pages/Doctors";
import { Nurses} from "../Pages/Nurses";
import { PatientAssociation} from "../Pages/PatientAssociation"
import { Profile} from "../Pages/Profile";
import { DoctorRegistration} from "../Pages/DoctorRegistration";
import { NurseRegistration} from "../Pages/NurseRegistration";
class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <div>
                <div className="container">
                    <div className="col-sm-8 col-sm-offset-2">
                        {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
                        <Router history={history}>
                            <Switch>
                                <PrivateRoute exact path="/" component={Patients}/>
                                <Route path="/login" component={LoginPage} />
                                <Route path="/register" component={RegisterPage} />
                                <Route path="/dashboard" component={DashBoard} />
                                <Route path="/patients" component={Patients} />
                                <Route path="/doctors" component={Doctors} />
                                <Route path="/patientassociation" component={PatientAssociation} />
                                <Route path="/nurses" component={Nurses} />
                                <Route path="/doctorregistration" component={DoctorRegistration} />
                                <Route path="/nurseregistration" component={NurseRegistration} />
                                <Route path="/profile" component={Profile} />
                                <Redirect from="*" to="/" />
                            </Switch>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };