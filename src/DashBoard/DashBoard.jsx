import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Appbar from "../DashBoard/components/Appbar";
import LeftDrawer from "../DashBoard/components/LeftDrawer";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { userActions } from '../_actions';
import { makeStyles } from '@material-ui/core/styles';

class DashBoard extends React.Component {
    componentDidMount() {
        this.props.getUsers();
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }
    render() {
        const { user, users } = this.props;
        return (
            <div>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Paper>
                            <LeftDrawer/>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }

}
 
function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(DashBoard);
export { connectedHomePage as DashBoard };