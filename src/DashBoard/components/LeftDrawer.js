import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PlusIcon from '@material-ui/icons/AddBox';
import UserIcon from '@material-ui/icons/AccessibleSharp';
import LogoutIcon from '@material-ui/icons/Input';
import UserPMIcon from '@material-ui/icons/DesktopWindows';
import ProfileIcon from '@material-ui/icons/AccountCircle';
import { history } from '../../_helpers';
const drawerWidth = 240;
let user = JSON.parse(localStorage.getItem('user'));
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    background:'transparent'
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const handleListItemClick = (e,text,index) => {
    setSelectedIndex(index);
    console.log(user)
    e.preventDefault();
    
    if(text==="Patients"){
        history.push('/patients')
    }else if(text === "Doctors"){
        history.push('/doctors')
    }
    else if(text === "Nurses"){
        history.push('/nurses')
    }
    else if(text === "PatientAssociation"){
        history.push('/patientAssociation')
    }
    else if(text === "Profile"){
        history.push('/profile')
    }
    else if(text === "Logout"){
        history.push('/Login')
    }
  };
  
  return (
    
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar)}
      >
        <Toolbar>
          <Typography variant="h6" noWrap>
            dtx-Covid-19
          </Typography>
          {/* <Link to="/login">Logout</Link> */}
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        anchor="left"
        classes={{
          paper: classes.drawerPaper,
        }}
      > 
        <div style={{textAlign:"center"}}>
        <h4>{user.username}</h4>
        <h6>Admin</h6>
        </div>
        <Divider />
        <Divider />
        <List>
          <ListItem button key="Patients" onClick={e => handleListItemClick(e,"Patients",1)} selected={selectedIndex === 1}>
              <ListItemIcon><UserIcon /></ListItemIcon>
              <ListItemText primary="Patients" />
          </ListItem>
          <Divider />
          <ListItem button key="Doctors" onClick={e => handleListItemClick(e,"Doctors",2)} selected={selectedIndex === 2}>
              <ListItemIcon><PlusIcon /></ListItemIcon>
              <ListItemText primary="Doctors" />
          </ListItem>
          <Divider />
          <ListItem button key="Nurses" onClick={e => handleListItemClick(e,"Nurses",4)} selected={selectedIndex === 3}>
              <ListItemIcon><PlusIcon /></ListItemIcon>
              <ListItemText primary="Nurses" />
          </ListItem>
          <Divider />
          <ListItem button key="PatientAssociation" onClick={e => handleListItemClick(e,"PatientAssociation",3)} selected={selectedIndex === 4}>
              <ListItemIcon><UserPMIcon /></ListItemIcon>
              <ListItemText primary="Patient Association" />
          </ListItem>
          <Divider />
          <ListItem button key="Profile" onClick={e => handleListItemClick(e,"Profile",5)} selected={selectedIndex === 5}>
              <ListItemIcon><ProfileIcon /></ListItemIcon>
              <ListItemText primary="Profile" />
          </ListItem>
          <Divider />
          <ListItem button key="Logout" onClick={e => handleListItemClick(e,"Logout",6)} selected={selectedIndex === 6}>
              <ListItemIcon><LogoutIcon /></ListItemIcon>
              <ListItemText primary="Logout" />
          </ListItem>
          <Divider />
          
        </List>
      </Drawer>
    </div>
  );
}