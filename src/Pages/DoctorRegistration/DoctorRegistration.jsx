import React from "react";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { userActions } from "../../_actions";
import Button from "@material-ui/core/Button";
import { history } from "../../_helpers";
class DoctorRegistration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        firstName: "",
        lastName: "",
        username: "",
        password: "",
        reenterpassword: "",
        email: "",
        designation: "",
        specialization: "",
        phonenumber: "",
        hospital: "",
        city: "",
        country: "",
      },
      submitted: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

 

  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value,
      },
    });
  }

  handleSubmit(event) {
    debugger;
    event.preventDefault();

    this.setState({ submitted: true });
    const { user } = this.state;
    if (
      user.firstName &&
      user.lastName &&
      user.username &&
      user.password &&
      user.email &&
      user.reenterpassword &&
      user.specialization &&
      user.designation &&
      user.phonenumber &&
      user.hospital &&
      user.city &&
      user.country

    ) {
      this.props.addDoctor(user);
    }
  }

  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  handleBackButton(e) {
    e.preventDefault();
    history.push("/doctors");
  }
  render() {
    // const { user, users } = this.props;
    const { registering } = this.props;
    const { user, submitted } = this.state;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper>
              <LeftDrawer />
            </Paper>
            <Paper style={{ width: "120%", height: "60%" }}>
              <main
                style={{
                  flexGrow: 1,
                  backgroundColor: "white",
                  padding: 10,
                  fontSize: 30,
                }}
              >
                <div>
                  <div style={{ marginTop: "5%" }}>
                    <div>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={(e) => this.handleBackButton(e)}
                      >
                        Back
                      </Button>
                      <h4 style={{ textAlign: "center" }}>
                        Doctor Registration
                      </h4>
                    </div>
                    <div style={{ marginLeft: "30%" }}>
                      <form name="form" onSubmit={this.handleSubmit}>
                      
                      <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="firstName" style={{fontSize:10}}>First Name</label>
                        <input type="text" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} style={{height:'2%',width:'90%'}}/>
                        {/* <TextField id="standard-basic" label="FirstName" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} /> */}
 
                        {submitted && !user.firstName &&
                            <div className="help-block">First Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                        <label htmlFor="lastName" style={{fontSize:10}}>Last Name</label>
                        <input type="text" className="form-control" name="lastName" value={user.lastName} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.lastName &&
                            <div className="help-block">Last Name is required</div>
                        }
                    </div>
                      
                      
                    <div className={'form-group' + (submitted && !user.email ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="email" style={{fontSize:10}}>E-mail</label>
                        <input type="text" className="form-control" name="email" value={user.email} onChange={this.handleChange} style={{height:'2%',width:'90%'}} />
                        {submitted && !user.email &&
                            <div className="help-block">Email is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.phonenumber ? ' has-error' : '')}>
                        <label htmlFor="phonenumber" style={{fontSize:10}}>Phone Number</label>
                        <input type="text" className="form-control" name="phonenumber" value={user.phonenumber} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.phonenumber &&
                            <div className="help-block">Email is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="username" style={{fontSize:10}}>Username</label>
                        <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} style={{height:'2%',width:'90%'}} />
                        {submitted && !user.username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                        <label htmlFor="password" style={{fontSize:10}}>Password</label>
                        <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.reenterpassword ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="reenterpassword" style={{fontSize:10}}>Re-enter Password</label>
                        <input type="password" className="form-control" name="reenterpassword" value={user.reenterpassword} onChange={this.handleChange} style={{height:'2%',width:'90%'}} />
                        {submitted && !user.reenterpassword &&
                            <div className="help-block">Re-Enter Password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.designation ? ' has-error' : '')}>
                        <label htmlFor="designation" style={{fontSize:10}}>Designation</label>
                        <input type="text" className="form-control" name="designation" value={user.designation} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.designation &&
                            <div className="help-block">Designation is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.specialization ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="specialization" style={{fontSize:10}}>Specialization</label>
                        <input type="text" className="form-control" name="specialization" value={user.specialization} onChange={this.handleChange} style={{height:'2%',width:'90%'}} />
                        {submitted && !user.specialization &&
                            <div className="help-block">Specialization is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.hospital ? ' has-error' : '')}>
                        <label htmlFor="hospital" style={{fontSize:10}}>Hospital</label>
                        <input type="text" className="form-control" name="hospital" value={user.hospital} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.hospital &&
                            <div className="help-block">Hospital is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.city ? ' has-error' : '')} style={{display:'inline-block',float:'left'}}>
                        <label htmlFor="city" style={{fontSize:10}}>City</label>
                        <input type="text" className="form-control" name="city" value={user.city} onChange={this.handleChange} style={{height:'2%',width:'90%'}} />
                        {submitted && !user.city &&
                            <div className="help-block">City is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.country ? ' has-error' : '')}>
                        <label htmlFor="country" style={{fontSize:10}}>Country</label>
                        <input type="text" className="form-control" name="country" value={user.country} onChange={this.handleChange} style={{height:'2%',width:'30%'}} />
                        {submitted && !user.country &&
                            <div className="help-block">Country is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary" style={{marginLeft:'25%'}}>Register</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        
                    </div>
                      </form>
                    </div>
                  </div>
                </div>
              </main>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapState(state) {
  const { registering } = state.registration;
    return { registering };
}

const actionCreators = {
  addDoctor: userActions.addDoctor
};

const connectedDoctorRegistrationPage = connect(
  mapState,
  actionCreators
)(DoctorRegistration);
export { connectedDoctorRegistrationPage as DoctorRegistration };
