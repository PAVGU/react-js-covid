import React from "react";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { userActions } from "../../_actions";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import { history } from "../../_helpers";
class Doctors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docs: [],
    };
  }

  componentWillMount() {
    // fetch("http://localhost:4000/doctors")
    // .then(response=>response.json())
    // .then(data=> this.setState({ docs : data.docs}))
    // .catch(e=>{
    //   console.log(e);

    // })
    // console.log(this.state.docs)
    this.setState({ docs: JSON.parse(localStorage.getItem("doctors")) });
  }

  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  handleAddButtonClick(e) {
    e.preventDefault();
    history.push("/doctorregistration");
  }
  render() {
    console.log(this.state.docs);
    const { docs } = this.state;
    if (docs === null) {
      return (
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper>
                <LeftDrawer />
              </Paper>
              <Paper
                style={{ marginLeft: "8%", width: "120%", height: "120%" }}
              >
                <main
                  style={{
                    flexGrow: 1,
                    backgroundColor: "white",
                    padding: 10,
                    fontSize: 30,
                  }}
                >
                  <div>
                    <div style={{ marginTop: "8%" }}>
                      <div>
                        <h4 style={{ textAlign: "center" }}>Doctors</h4>
                        <Button
                          variant="contained"
                          color="primary"
                          style={{
                            float: "right",
                            marginTop: "-3%",
                            marginBottom: "3%",
                          }}
                          onClick={(e) => this.handleAddButtonClick(e)}
                        >
                          ADD
                        </Button>
                      </div>
                      <TableContainer>
                        <Table>
                          <TableHead style={{ backgroundColor: "silver" }}>
                            <TableRow>
                              <TableCell>FirstName</TableCell>
                              <TableCell align="right">LastName</TableCell>
                              <TableCell align="right">
                                Specialization
                              </TableCell>
                              <TableCell align="right">Designation</TableCell>
                              <TableCell align="right">City</TableCell>
                              <TableCell align="right">Country</TableCell>
                              <TableCell align="right">PhoneNumber</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow>
                              <TableCell>No Data</TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                  </div>
                </main>
              </Paper>
            </Grid>
          </Grid>
        </div>
      );
    } else {
      return (
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper>
                <LeftDrawer />
              </Paper>
              <Paper
                style={{ marginLeft: "8%", width: "120%", height: "120%" }}
              >
                <main
                  style={{
                    flexGrow: 1,
                    backgroundColor: "white",
                    padding: 10,
                    fontSize: 30,
                  }}
                >
                  <div>
                    <div style={{ marginTop: "8%" }}>
                      <div>
                        <h4 style={{ textAlign: "center" }}>Doctors</h4>
                        <Button
                          variant="contained"
                          color="primary"
                          style={{
                            float: "right",
                            marginTop: "-3%",
                            marginBottom: "3%",
                          }}
                          onClick={(e) => this.handleAddButtonClick(e)}
                        >
                          ADD
                        </Button>
                      </div>
                      <TableContainer>
                        <Table>
                          <TableHead style={{ backgroundColor: "silver" }}>
                            <TableRow>
                              <TableCell>FirstName</TableCell>
                              <TableCell align="right">LastName</TableCell>
                              <TableCell align="right">
                                Specialization
                              </TableCell>
                              <TableCell align="right">Designation</TableCell>
                              <TableCell align="right">City</TableCell>
                              <TableCell align="right">Country</TableCell>
                              <TableCell align="right">PhoneNumber</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {docs.map((row) => (
                              <TableRow key={row.firstName}>
                                <TableCell component="th" scope="row">
                                  {row.firstName}
                                </TableCell>
                                <TableCell align="right">
                                  {row.lastName}
                                </TableCell>
                                <TableCell align="right">
                                  {row.specialization}
                                </TableCell>
                                <TableCell align="right">
                                  {row.designation}
                                </TableCell>
                                <TableCell align="right">{row.city}</TableCell>
                                <TableCell align="right">
                                  {row.country}
                                </TableCell>
                                <TableCell align="right">
                                  {row.phonenumber}
                                </TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                  </div>
                </main>
              </Paper>
            </Grid>
          </Grid>
        </div>
      );
    }
  }
}

function mapState(state) {
  const { doctors, authentication } = state;
  debugger;
  const { user } = authentication;
  return { user, doctors };
}

const actionCreators = {
  getDoctors: userActions.getDoctors,
  deleteUser: userActions.delete,
};

const connectedPatientsPage = connect(mapState, actionCreators)(Doctors);
export { connectedPatientsPage as Doctors };
