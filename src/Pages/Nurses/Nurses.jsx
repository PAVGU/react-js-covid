import React from "react";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { userActions } from "../../_actions";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from '@material-ui/core/Button';
import { history } from '../../_helpers';

class Nurses extends React.Component {
  constructor(props){
    super(props);
    this.state={
      data:[]
    }
  }
  
  componentWillMount(){
    // this.props.getUsers()
    this.setState({data:JSON.parse(localStorage.getItem('nurses'))})
  }
  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  handleAddButtonClick(e){
    e.preventDefault();
    history.push('/nurseregistration');
  }
  render() {
    const { data }  = this.state;
    if(data === null){
      return (
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper>
                <LeftDrawer />
              </Paper>
              <Paper style={{marginLeft:'8%',width:'120%',height:'120%'}}>
              <main
                  style={{
                    flexGrow: 1,
                    backgroundColor: "white",
                    padding: 10,
                    fontSize: 30,
                  }}
                >
                  <div>
                    <div style={{ marginTop: "10%" }}>
                    <div>
                      <h4 style={{textAlign:"center"}}>Nurses</h4><Button variant="contained" color="primary" style={{float:"right",marginTop:'-3%', marginBottom:'3%'}} onClick={e => this.handleAddButtonClick(e)}>ADD</Button>
                      </div>
                      <TableContainer>
                        <Table>
                          <TableHead style={{backgroundColor:"silver"}}>
                            <TableRow>
                              <TableCell>FirstName</TableCell>
                              <TableCell align="right">LastName</TableCell>
                              <TableCell align="right">Specialization</TableCell>
                              <TableCell align="right">Designation</TableCell>
                              <TableCell align="right">City</TableCell>
                              <TableCell align="right">Country</TableCell>
                              <TableCell align="right">PhoneNumber</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow>
                              <TableCell>
                                No Data
                              </TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                  </div>
                </main>
              </Paper>
            </Grid>
          </Grid>
        </div>
      );
    }else{
      return (
        <div>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper>
                <LeftDrawer />
              </Paper>
              <Paper style={{marginLeft:'8%',width:'120%',height:'120%'}}>
              <main
                  style={{
                    flexGrow: 1,
                    backgroundColor: "white",
                    padding: 10,
                    fontSize: 30,
                  }}
                >
                  <div>
                    <div style={{ marginTop: "10%" }}>
                    <div>
                      <h4 style={{textAlign:"center"}}>Nurses</h4><Button variant="contained" color="primary" style={{float:"right",marginTop:'-3%', marginBottom:'3%'}} onClick={e => this.handleAddButtonClick(e)}>ADD</Button>
                      </div>
                      <TableContainer>
                        <Table>
                          <TableHead style={{backgroundColor:"silver"}}>
                            <TableRow>
                              <TableCell>FirstName</TableCell>
                              <TableCell align="right">LastName</TableCell>
                              <TableCell align="right">Designation</TableCell>
                              <TableCell align="right">City</TableCell>
                              <TableCell align="right">Country</TableCell>
                              <TableCell align="right">PhoneNumber</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {data.map((row) => (
                              <TableRow key={row.firstName}>
                                <TableCell component="th" scope="row">
                                  {row.firstName}
                                </TableCell>
                                <TableCell align="right">
                                  {row.lastName}
                                </TableCell>
                                <TableCell align="right">{row.designation}</TableCell>
                                <TableCell align="right">{row.city}</TableCell>
                                <TableCell align="right">{row.country}</TableCell>
                                <TableCell align="right">
                                  {row.phonenumber}
                                </TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </div>
                  </div>
                </main>
              </Paper>
            </Grid>
          </Grid>
        </div>
      );
    }

    
  }
}

function mapState(state) {
  const { nurses, authentication } = state;
  const { user } = authentication;
  return { user, nurses };
}

const actionCreators = {
  getUsers: userActions.getNurses,
  deleteUser: userActions.delete,
};

const connectedNursePage = connect(mapState, actionCreators)(Nurses);
export { connectedNursePage as Nurses };
