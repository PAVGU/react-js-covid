import React from "react";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { userActions } from "../../_actions";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import Button from '@material-ui/core/Button';
// import { Switch } from "@material-ui/core";
class PatientAssociation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      patient: "",
      doctor:"",
      nurse:""
    };
  }
  componentDidMount() {
    this.props.getUsers();
  }

  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  handleChange(event) {
    this.setState({patient : event.target.value})
  }
  handleDoctorChange(event){
    this.setState({doctor : event.target.value})
  }
  handleNurseChange(event){
    this.setState({nurse : event.target.value})
  }
  render() {
    // const { user, users } = this.props;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper>
              <LeftDrawer />
            </Paper>
            <Paper style={{marginLeft:'8%',width:'120%',height:'120%'}}>
              <main style={{ flexGrow: 1, padding: 30 }}>
                <div>
                  <div style={{ marginTop: "10%", flexGrow: 1 }}>
                    <h4 style={{ textAlign: "center",textDecoration:'underline',color:'blue' }}><strong>PATIENT ASSOCIATION</strong></h4>
                    <br/>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <FormControl
                          variant="outlined"
                          style={{ width: "20%", marginLeft: "40%" }}
                        >
                          <InputLabel id="demo-simple-select-outlined-label">
                            Patient Name
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={this.state.patient}
                            onChange={e => this.handleChange(e)}
                            label="PatientName"
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value={'P1'}>PatientOne</MenuItem>
                            <MenuItem value={"P2"}>PatientTwo</MenuItem>
                            <MenuItem value={'P3'}>PatientThree</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <br/>
                      <Grid item xs={6}>
                        <FormControl
                          variant="outlined"
                          style={{ width: "40%", marginLeft: "40%" }}
                        >
                          <InputLabel id="demo-simple-select-outlined-label">
                            Doctor Name
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={this.state.doctor}
                            onChange={e => this.handleDoctorChange(e)}
                            label="DoctorName"
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value={'D1'}>DoctorOne</MenuItem>
                            <MenuItem value={'D2'}>DoctorTwo</MenuItem>
                            <MenuItem value={'D3'}>DoctorThree</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <br/>
                      <Grid item xs={6}>
                        <FormControl
                          variant="outlined"
                          style={{ width: "40%", marginLeft: "20%" }}
                        >
                          <InputLabel id="demo-simple-select-outlined-label">
                            Nurse Name
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={this.state.nurse}
                            onChange={e => this.handleNurseChange(e)}
                            label="NurseName"
                          >
                            <MenuItem value="">
                              <em>None</em>
                            </MenuItem>
                            <MenuItem value={'N1'}>NurseOne</MenuItem>
                            <MenuItem value={'N2'}>NurseTwo</MenuItem>
                            <MenuItem value={'N3'}>NurseThree</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <br/>
                      <Grid item xs={12}>
                        <Button variant="contained" color="primary" style={{marginLeft:'45%'}}>Save</Button>
                      </Grid>
                    </Grid>
                  </div>
                </div>
              </main>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapState(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return { user, users };
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete,
};

const connectedPatientAssocPage = connect(
  mapState,
  actionCreators
)(PatientAssociation);
export { connectedPatientAssocPage as PatientAssociation };
