import React from "react";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { userActions } from "../../_actions";
// import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

class Patients extends React.Component {
  // componentDidMount() {
  //   this.props.getUsers();
  // }

  createData(FirstName, LastName, Age, Weight, City, Country, PhoneNumber) {
    return { FirstName, LastName, Age, Weight, City, Country, PhoneNumber };
  }

  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  render() {
    // const { user, users } = this.props;
    const rows = [
      this.createData(
        "Patient",
        "One",
        "35",
        72.5,
        "California",
        "United States",
        "+134543533"
      ),
      this.createData(
        "Patient",
        "Two",
        "45",
        62.7,
        "Maryland",
        "United States",
        "+143533453"
      ),
      this.createData(
        "Patient",
        "Three",
        "53",
        84.2,
        "Utah",
        "United States",
        "+143533453"
      ),
      this.createData(
        "Patient",
        "Four",
        "50",
        55.9,
        "Nevada",
        "United States",
        "+143533453"
      ),
      this.createData(
        "Patient",
        "Five",
        "67",
        67.8,
        "California",
        "United States",
        "+143533453"
      ),
    ];
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper>
              <LeftDrawer />
            </Paper>
            <Paper style={{marginLeft:'8%',width:'120%',height:'120%'}}>
              <main
                style={{
                  flexGrow: 1,
                  backgroundColor: "white",
                  padding: 10,
                  fontSize: 30,
                }}
              >
                <div>
                  <div style={{ marginTop: "10%" }}>
                  <div>
                    <h4 style={{textAlign:"center"}}>Patients</h4>
                    </div>
                    <TableContainer>
                      <Table>
                        <TableHead style={{backgroundColor:"silver"}}>
                          <TableRow>
                            <TableCell>FirstName</TableCell>
                            <TableCell align="right">LastName</TableCell>
                            <TableCell align="right">Age</TableCell>
                            <TableCell align="right">Weight(kgs)</TableCell>
                            <TableCell align="right">City</TableCell>
                            <TableCell align="right">Country</TableCell>
                            <TableCell align="right">PhoneNumber</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {rows.map((row) => (
                            <TableRow key={row.FirstName}>
                              <TableCell component="th" scope="row">
                                {row.FirstName}
                              </TableCell>
                              <TableCell align="right">
                                {row.LastName}
                              </TableCell>
                              <TableCell align="right">{row.Age}</TableCell>
                              <TableCell align="right">{row.Weight}</TableCell>
                              <TableCell align="right">{row.City}</TableCell>
                              <TableCell align="right">{row.Country}</TableCell>
                              <TableCell align="right">
                                {row.PhoneNumber}
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </div>
                </div>
              </main>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapState(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return { user, users };
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete,
};

const connectedPatientsPage = connect(mapState, actionCreators)(Patients);
export { connectedPatientsPage as Patients };
