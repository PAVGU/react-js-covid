import React from "react";
import { connect } from "react-redux";
import LeftDrawer from "../../DashBoard/components/LeftDrawer";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { userActions } from "../../_actions";
import Button from '@material-ui/core/Button';

class Profile extends React.Component {
  componentDidMount() {
    this.props.getUsers();
  }

  handleDeleteUser(id) {
    return (e) => this.props.deleteUser(id);
  }
  render() {
    const { user } = this.props;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper>
              <LeftDrawer />
            </Paper>
            <Paper style={{marginLeft:'15%'}}>
              <main
                style={{ flexGrow: 1, backgroundColor: "white", padding: 10 }}
              >
                <div>
                  <div style={{ marginTop: "10%"}}>
                    <div>
                      <h4 style={{ textAlign: "center", marginTop:'15%' }}>Profile</h4>
                      <div style={{ marginLeft: "10%", padding: 10 }}>
                        <TextField
                          id="outlined-basic"
                          label="FirstName"
                          variant="outlined"
                          value={user.firstName}
                          style={{ padding: 10, width: "40%" }}
                        />
                        <TextField
                          id="outlined-basic"
                          label="LastName"
                          variant="outlined"
                          value={user.lastName}
                          style={{ padding: 10, width: "40%" }}
                        />
                        <br />
                        <TextField
                          id="outlined-basic"
                          label="Email"
                          variant="outlined"
                          value={user.email}
                          style={{ padding: 10, width: "40%" }}
                        />
                        <TextField
                          id="outlined-basic"
                          label="UserName"
                          variant="outlined"
                          value={user.username}
                          style={{ padding: 10, width: "40%" }}
                        />
                        <br />
                        <Button variant="contained" color="primary" style={{marginLeft:'33%'}}>Update</Button>
                      </div>
                    </div>
                  </div>
                </div>
              </main>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapState(state) {
  const { users, authentication } = state;
  debugger;
  const { user } = authentication;
  return { user, users };
}

const actionCreators = {
  getUsers: userActions.getAll,
  deleteUser: userActions.delete,
};

const connectedProfilePage = connect(mapState, actionCreators)(Profile);
export { connectedProfilePage as Profile };
