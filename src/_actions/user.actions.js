import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    getAll,
    getDoctors,
    getNurses,
    addDoctor,
    addNurse,
    delete: _delete
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        userService.login(username, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function addDoctor(doctor){
    debugger;
    return dispatch => {
        dispatch(request(doctor));

        userService.addDoctor(doctor)
            .then(
                doctor => { 
                    dispatch(success());
                    history.push('/doctors');
                    dispatch(alertActions.success('Doctor Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(doctor) { return { type: userConstants.DOCTOR_REGISTER_REQUEST, doctor } }
    function success(doctor) { return { type: userConstants.DOCTOR_SUCCESS_REQUEST, doctor } }
    function failure(error) { return { type: userConstants.DOCTORS_FAILURE_REQUEST, error } }
}

function addNurse(nurse){
    debugger;
    return dispatch => {
        dispatch(request(nurse));

        userService.addNurse(nurse)
            .then(
                nurse => { 
                    dispatch(success());
                    history.push('/nurses');
                    dispatch(alertActions.success('Nurse Registration successful'));
                    
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(nurse) { return { type: userConstants.NURSE_REGISTER_REQUEST, nurse } }
    function success(nurse) { return { type: userConstants.NURSE_SUCCESS_REQUEST, nurse } }
    function failure(error) { return { type: userConstants.NURSE_FAILURE_REQUEST, error } }
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getNurses(){
    debugger;
    return dispatch => {
        dispatch(request());

        userService.getNurses()
            .then(
                nurses => dispatch(success(nurses)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_NURSE_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_NURSE_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_NURSE_FAILURE, error } }
}

function getAll() {
    debugger;
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

function getDoctors(){
    debugger;
    return dispatch => {
        dispatch(request());

        userService.getDoctors()
            .then(
                doctors => dispatch(success(doctors)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_DOCTOR_REQUEST } }
    function success(doctors) { return { type: userConstants.GETALL_DOCTOR_SUCCESS, doctors } }
    function failure(error) { return { type: userConstants.GETALL_DOCTOR_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}