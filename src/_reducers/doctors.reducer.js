import { userConstants } from '../_constants';

export function doctors(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_DOCTOR_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_DOCTOR_SUCCESS:
      return {
        items: action.doctors
      };
    case userConstants.GETALL_DOCTOR_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}