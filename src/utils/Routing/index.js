import { isWeb } from '../common';
console.log('in routing file');

const RouterPackage = isWeb
  ? require('react-router-dom')
  : require('react-router-native');

export const Router = isWeb
  ? RouterPackage.BrowserRouter
  : RouterPackage.NativeRouter;

export default RouterPackage;