import { Platform } from 'react-native'

export const isWeb = Platform.OS === 'web';


export const isTabBrowser = ()=> {
    return window.innerWidth <= 1200;
};

export const isMobileBrowser = ()=> {
    return window.innerWidth <= 930;
};